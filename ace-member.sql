create table acemember (
    first_name varchar(255) not null,
    last_name varchar(255) not null,
    uin varchar(255) not null primary key,
    family varchar(255) not null,
    major varchar(255) not null,
    position varchar(255) default 'member' not null,
    phone  varchar(255) not null,
    e_mail varchar(255) not null,
    p_word varchar(255) not null
);