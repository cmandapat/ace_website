create table ace_events (
    creator_uin varchar(255) not null,
    event_name varchar(255) not null,
    event_id varchar(255) not null primary key auto_increment,
    event_type varchar(255) not null,
    event_points varchar(255) not null,
    event_start varchar(255) not null,
    event_end  varchar(255) not null
);