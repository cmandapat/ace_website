create table ace_session (
    uuid varchar(255) primary not null,
    uin varchar(255) not null,
    time_stamp varchar(255) not null
);