package org.aggieclubofengineers.website;

import io.dropwizard.views.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



public class DashboardView extends View {
    private String username;

    public DashboardView() { super("/assets/dashboard.mustache");}

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
