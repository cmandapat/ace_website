package org.aggieclubofengineers.website;

import io.dropwizard.hibernate.AbstractDAO;
import lombok.Builder;
import lombok.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;
import java.util.List;


public class UserAccountDAO extends AbstractDAO<UserAccount> {
    private final String CREATE_ACCOUNT_QUERY = "INSERT INTO acemember (first_name,last_name,family,major,p_word,e_mail,uin,phone) VALUES (:f_name,:l_name,:family,:major,:password,:email,:uin,:phone);";

    private final String CHECK_LOGIN_QUERY = "SELECT * from acemember where uin = :uin";

    /**
     * Constructor
     *
     * @param sessionFactory Hibernate session factory
     * */

    public UserAccountDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public UserAccount checkUserLogin(@NonNull String uin, @NonNull String password) {
        Query query = currentSession().createNativeQuery(CHECK_LOGIN_QUERY,UserAccount.class);
        query.setParameter("uin",uin);
        List<UserAccount> userAccountList = (List<UserAccount>) query.getResultList();
        UserAccount userAccount = userAccountList.get(0);

        if(BCrypt.checkpw(password,userAccount.getPassword()) && userAccountList.size() != 0) {
            return userAccount;
        } else {
            return null;
        }


    }

    public UserAccount findByUIN(@NonNull String uin) {
        Query query = currentSession().createNativeQuery(CHECK_LOGIN_QUERY,UserAccount.class);
        query.setParameter("uin",uin);
        List<UserAccount> userUIN = (List<UserAccount>) query.getResultList();
        return  userUIN.get(0);

    }

    public Boolean createUserAccount(@NonNull String f_name, @NonNull String l_name,
                                    @NonNull String major, @NonNull String password,
                                     @NonNull String email,@NonNull String uin, @NonNull String phone,
                                     @NonNull String family){
        UserAccount account = UserAccount.builder()
                        .f_name(f_name)
                        .l_name(l_name)
                        .password(password)
                        .email(email)
                        .uin(uin)
                        .phone(phone)
                        .major(major)
                        .family(family)
                        .build();

        Query query = currentSession().createNativeQuery(CREATE_ACCOUNT_QUERY);


        try {
            query.setParameter("f_name", account.getF_name());
            query.setParameter("l_name", account.getL_name());
            query.setParameter("password", BCrypt.hashpw(account.getPassword(),BCrypt.gensalt()));
            query.setParameter("email",account.getEmail());
            query.setParameter("uin",account.getUin());
            query.setParameter("phone",account.getPhone());
            query.setParameter("major",account.getMajor());
            query.setParameter("family", account.getFamily());
            return query.executeUpdate() == 1;
        }catch (Exception e) {
            System.out.println("This Account already exists!");
            return false;
        }






    }
}
