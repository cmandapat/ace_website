package org.aggieclubofengineers.website;


import io.dropwizard.views.View;

public class HomeView extends View {
    public HomeView() {
        super("/assets/homepage.mustache");
    }
}
