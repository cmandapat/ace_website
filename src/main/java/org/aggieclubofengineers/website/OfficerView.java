package org.aggieclubofengineers.website;
import io.dropwizard.views.View;
public class OfficerView extends View{
    public OfficerView() {
        super("/assets/officer.mustache");
    }
}
