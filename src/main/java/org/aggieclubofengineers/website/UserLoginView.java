package org.aggieclubofengineers.website;


import io.dropwizard.views.View;

public class UserLoginView extends View {
    public UserLoginView() {super("/assets/member-login.mustache");}
}
