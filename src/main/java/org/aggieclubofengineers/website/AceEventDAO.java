package org.aggieclubofengineers.website;

import io.dropwizard.hibernate.AbstractDAO;
import lombok.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class AceEventDAO extends AbstractDAO<AceEvent>{
    String CREATE_EVENT_QUERY = "INSERT INTO ace_events (creator_uin,event_name,event_type,event_points,event_start,event_end) VALUES (:creator_uin,:event_name,:event_type,:event_points,:event_start,:event_end);";
    String SELECT_USER_EVENT_QUERY = "SELECT * from ace_events where creator_uin = :uin";


    public AceEventDAO(SessionFactory sessionFactory) {super(sessionFactory);}

    public Boolean createAceEvent(@NonNull String creator_uin, @NonNull String event_name,
                                  @NonNull String event_type, @NonNull int event_points,
                                  @NonNull String event_start, @NonNull String event_end) {

        AceEvent event = AceEvent.builder()
                        .creator_uin(creator_uin)
                        .event_name(event_name)
                        .event_type(event_type)
                        .event_points(event_points)
                        .event_start(event_start)
                        .event_end(event_end)
                        .build();
        Query query = currentSession().createNativeQuery(CREATE_EVENT_QUERY);
        try {
            query.setParameter("creator_uin",creator_uin);
            query.setParameter("event_name",event.getEvent_name());
            query.setParameter("event_type",event.getEvent_type());
            query.setParameter("event_points",event.getEvent_points());
            query.setParameter("event_start",event.getEvent_start());
            query.setParameter("event_end",event.getEvent_end());
            return query.executeUpdate() == 1;

        }catch (Exception e) {
            System.out.println("Cannot Create Event");
            return false;
        }
    }

    public List<AceEvent> displayUserCreatedEvents (@NonNull String uin) {
        Query query = currentSession().createNativeQuery(SELECT_USER_EVENT_QUERY,AceEvent.class);
        query.setParameter("uin",uin);
        List<AceEvent> userCreatedEvent = query.getResultList();
        if(userCreatedEvent == null || userCreatedEvent.isEmpty()) {
            return null;
        }
        return userCreatedEvent;
    }
}
