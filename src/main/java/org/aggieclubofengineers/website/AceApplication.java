package org.aggieclubofengineers.website;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.dropwizard.assets.AssetsBundle;

public class AceApplication extends Application<AceConfiguration>{

    public static void main(String[] args) throws Exception {
        new AceApplication().run(args);
    }

    private final HibernateBundle<AceConfiguration> hibernateBundle = new HibernateBundle<AceConfiguration>(UserAccount.class,AceEvent.class,AceSession.class)
    {
        @Override
        public DataSourceFactory getDataSourceFactory(
                AceConfiguration configuration
        ) {
            return configuration.getDataSourceFactory();
        }
    };



    @Override
    public void initialize(Bootstrap<AceConfiguration> bootstrap) {
        // nothing to do yet
        bootstrap.addBundle(new ViewBundle());
        bootstrap.addBundle(new AssetsBundle("/assets","/assets"));
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(AceConfiguration configuration,
                    Environment environment) {

        final UserAccountDAO userAccountDAO = new UserAccountDAO(hibernateBundle.getSessionFactory());
        final AceEventDAO aceEventDAO = new AceEventDAO(hibernateBundle.getSessionFactory());
        final SessionDAO sessionDAO = new SessionDAO(hibernateBundle.getSessionFactory());

        final AceResource resource = new AceResource(aceEventDAO,userAccountDAO,sessionDAO);
//        final AceSessionResource sessionResource = new AceSessionResource(userAccountDAO);
        environment.jersey().register(resource);
//        environment.jersey().register(sessionResource);

    }
}
