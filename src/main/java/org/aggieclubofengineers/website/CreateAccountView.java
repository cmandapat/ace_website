package org.aggieclubofengineers.website;
import io.dropwizard.views.View;

public class CreateAccountView extends View {
    public CreateAccountView () {
        super("/assets/create-account.mustache");
    }
}
