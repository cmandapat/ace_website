package org.aggieclubofengineers.website;




import io.dropwizard.views.View;

public class AdmissionsView extends View {
    public AdmissionsView() {
        super("/assets/admissions.mustache");
    }
}
