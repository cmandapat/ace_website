package org.aggieclubofengineers.website;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "ace_session")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor

public class AceSession {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private String id;
    @Id
    private String uuid;
    private String uin;
    @Column(name = "time_stamp")
    private String timestamp;
}
