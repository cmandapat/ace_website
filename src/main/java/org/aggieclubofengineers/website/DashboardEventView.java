package org.aggieclubofengineers.website;
import io.dropwizard.views.View;

import java.util.List;

public class DashboardEventView extends View {
    private List<AceEvent> userCreatedEvents;
    private List<AceEvent> listOfAllEvents;
    private String message;

    public DashboardEventView() { super("/assets/dashboard-events.mustache");}

    public void setListOfAllEvents(List<AceEvent> listOfAllEvents) {
        this.listOfAllEvents = listOfAllEvents;
    }
    public void setUserCreatedEvents(List<AceEvent> userCreatedEvents) {
        this.userCreatedEvents = userCreatedEvents;
    }
    public List<AceEvent> getUserCreatedEvents() { return this.userCreatedEvents;}

    public void setMessage(String message) {
        this.message = message;
    }
}
