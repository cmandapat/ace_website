package org.aggieclubofengineers.website;
import io.dropwizard.views.View;

public class CreateEventView extends View {

    public CreateEventView() {
        super("/assets/create-event.mustache");
    }
}
