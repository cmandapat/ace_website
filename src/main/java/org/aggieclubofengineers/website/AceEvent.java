package org.aggieclubofengineers.website;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "ace_events")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor

public class AceEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long event_id;
    private String creator_uin;
    private String event_name;
    private String event_type;
    private int event_points;
    private String event_start;
    private String event_end;

}
