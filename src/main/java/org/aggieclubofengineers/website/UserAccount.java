package org.aggieclubofengineers.website;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Entity;

import javax.persistence.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@javax.persistence.Entity
@Table(name = "acemember")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount {

   @Column(name = "first_name")
   private String f_name;
   @Column(name = "last_name")
   private String l_name;
   @Column(name = "p_word")
   private String password;
   @Column(name = "e_mail")
   private String email;
   @Id
   private String uin;
   private String phone;
   private String family;
   private String major;




}
