package org.aggieclubofengineers.website;

import com.mysql.jdbc.exceptions.MySQLDataException;
import com.mysql.jdbc.log.Log;
import io.dropwizard.hibernate.UnitOfWork;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static javax.management.timer.Timer.ONE_DAY;


@Path("/ace")
@Produces(MediaType.TEXT_HTML)

public class AceResource {
    private UserAccountDAO userAccountDAO;
    private DashboardView dbView;
    private AceEventDAO aceEventDAO;
    private SessionDAO sessionDAO;
    private DashboardEventView dbEventView;

    public AceResource(AceEventDAO aceEventDAO,UserAccountDAO userAccountDAO,SessionDAO sessionDAO) {
        this.aceEventDAO = aceEventDAO;
        this.userAccountDAO = userAccountDAO;
        this.sessionDAO = sessionDAO;
    }

    @GET
    @Path("/home")
    public HomeView homepage() {
        return new HomeView();
    }
    @GET
    @Path("/admission")
    public AdmissionsView admissionpage(){
        return new AdmissionsView();
    }
    @GET
    @Path("/cookingforhope")
    public CFHView cookingforhopepage() {
        return new CFHView();
    }
    @GET
    @Path("/officers")
    public OfficerView officerpage(){
        return new OfficerView();
    }

    @GET
    @Path("/create-account")
    public CreateAccountView createAccount() {return new CreateAccountView();}

    @GET
    @Path("/member-login")
    public UserLoginView memberLogin() {
        return new UserLoginView();
    }

    @GET
    @Path("/dashboard")
    public DashboardView dashboard() {
        return dbView;
    }

    @GET
    @Path("/dashboard-events")
    @UnitOfWork
    public DashboardEventView dashboardEventView(@Context HttpServletRequest response) {
        Cookie[] cookies = response.getCookies();
        if(cookies != null) {
            for(Cookie cookie : cookies) {
                if(cookie.getName().equals("makesession")) {
                    dbEventView = new DashboardEventView();
                    String sessionCookie = cookie.getValue();
                    AceSession session = sessionDAO.findSessionByUUID(sessionCookie);
                    UserAccount uin = userAccountDAO.findByUIN(session.getUin());
                    dbEventView.setUserCreatedEvents(aceEventDAO.displayUserCreatedEvents(uin.getUin()));

                }
            }
        }
        return dbEventView;
    }
//    @GET
//    @Path("/dashboard-events")
//    public Response dasboardEventView(@Context HttpServletResponse response) {
//
//    }

    @GET
    @Path("/create-event")
    public CreateEventView createEventView() {
        return new CreateEventView();
    }

    @POST
    @Path("/create-account")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response createAccount(UserAccount account) {
        userAccountDAO.createUserAccount(account.getF_name(),account.getL_name(),account.getMajor(),account.getPassword(),account.getEmail(),
                    account.getUin(),account.getPhone(),account.getFamily());
        return Response.ok().build(); // just returns a 200 status code to the browser
    }

    @POST
    @Path("/create-event")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response createEvent(@CookieParam("makesession") String sessionCookie,AceEvent event) {
        AceSession session = sessionDAO.findSessionByUUID(sessionCookie);
        UserAccount uin = userAccountDAO.findByUIN(session.getUin());

        aceEventDAO.createAceEvent(uin.getUin(),event.getEvent_name(),event.getEvent_type(),event.getEvent_points(),event.getEvent_start(),event.getEvent_end());
        return Response.ok().build();
    }

    @POST
    @Path("/member-login")
    @Produces(MediaType.TEXT_HTML)
    @UnitOfWork
    public Response checkLogin(UserAccount account) {
        Optional<UserAccount> userAccountOptional = Optional.ofNullable(userAccountDAO.checkUserLogin(account.getUin(), account.getPassword()));
        if(userAccountOptional.isPresent()) {
            account = userAccountOptional.get();
            Optional<AceSession> sessionOptional = sessionDAO.createSession(account.getUin());

            dbView = new DashboardView();
            dbView.setUsername(account.getF_name());

            if (!sessionOptional.isPresent()) {
                return Response.status(204).build();
            } else {
                AceSession session = sessionOptional.get();
                return Response.ok()
                        .cookie(
                                new NewCookie(
                                        "makesession",
                                        session.getUuid(),
                                        "/",
                                        null,
                                        1,
                                        "",
                                        (int) ONE_DAY,
                                        Date.from(Instant.now().plusSeconds(ONE_DAY)),
                                        false,
                                        false
                                )
                        )
                        .build();
            }
        } else {
            return Response.status(204).build();
        }
    }
}
