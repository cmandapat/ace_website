package org.aggieclubofengineers.website;

import io.dropwizard.hibernate.AbstractDAO;
import lombok.NonNull;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class SessionDAO extends AbstractDAO<AceSession> {
    String CREATE_SESSION_QUERY = "INSERT INTO ace_session (uuid,uin,time_stamp) VALUES (:uuid,:uin,:timestamp);";
    String SELECT_SESSION_QUERY = "SELECT * from ace_session where uuid = :uuid";
    public SessionDAO(SessionFactory sessionFactory) {super(sessionFactory);}

    public Optional<AceSession> createSession(@NonNull String uin) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        UUID randomUUID = UUID.randomUUID();
        String userUUID = randomUUID.toString();

        Timestamp createTimestamp = new Timestamp(System.currentTimeMillis());
        Date createDate = new Date();
        new Timestamp(createDate.getTime());
        String time = sdf.format(createTimestamp);

        AceSession aceSession = AceSession.builder()
                .uuid(userUUID)
                .uin(uin)
                .timestamp(time)
                .build();

        Query query = currentSession().createNativeQuery(CREATE_SESSION_QUERY);
        Optional<AceSession> aceSessionOptional = Optional.of(aceSession);
        try {
            query.setParameter("uuid", userUUID);
            query.setParameter("uin", uin);
            query.setParameter("timestamp", time);
            query.executeUpdate();
            return aceSessionOptional;
        } catch (Exception e) {
            System.out.println("Cannot create Session");
            return Optional.empty();
        }
    }

    public AceSession findSessionByUUID(@NonNull String uuid) {
        Query query = currentSession().createNativeQuery(SELECT_SESSION_QUERY,AceSession.class);
        query.setParameter("uuid",uuid);
        List<AceSession> session = query.getResultList();
        if(session == null || session.isEmpty()) {
            return null;
        }
        return session.get(0);
    }
}
