//package org.aggieclubofengineers.website;
//
//import io.dropwizard.hibernate.UnitOfWork;
//
//import javax.ws.rs.*;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//@Path("/ace")
//@Produces(MediaType.TEXT_HTML);
//public class AceSessionResource {
//    private UserAccountDAO userAccountDAO;
//    private DashboardView dbView;
//    public AceSessionResource(UserAccountDAO userAccountDAO) {
//        this.userAccountDAO = userAccountDAO;
//    }
//
//    @POST
//    @Path("/member-login")
//    @Produces(MediaType.TEXT_HTML)
//    @UnitOfWork
//    public DashboardView checkLogin(UserAccount account) {
//        account = userAccountDAO.checkUserLogin(account.getUin(),account.getPassword());
//        if (account != null) {
//            dbView = new DashboardView();
//            dbView.setUsername(account.getF_name());
//            return dbView;
//
//        } else {
//            return null;
//        }
//
//    }
//}
